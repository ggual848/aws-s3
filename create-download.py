import boto3
import botocore
#Create s3 object from boto3
s3 = boto3.resource('s3')

bucket = s3.create_bucket(
    ACL='public-read-write',
    Bucket='bucket-name-here',
    CreateBucketConfiguration={
        'LocationConstraint': 'us-east-2'},
)
#use collections to iterate over all items from data
#Use for loop to List names of buckets created
for bucket in s3.buckets.all():
    print(bucket.name)

#Using identifiers as parameters for action put_object
data = open('emoji.png', 'rb')
s3.Bucket('bucket-name-here').put_object(Key='test.png', Body=data)
s3.Bucket('bucket-name-here').put_object(Key='sample.txt', Body='Hello World!')
print("Uploaded.")
#to print out bucket objects
for bucket in s3.buckets.all():
    print (bucket.name)
    print ("---")
    for item in bucket.objects.all():
        print ("\t%s" % item.key)

BUCKET_NAME = 'bucket-name-here' # replace with your bucket name
KEY = 'sample.txt' # replace with your object key
#Use action download_file with object key
try:
    s3.Bucket(BUCKET_NAME).download_file(KEY, 'sample.txt',)
except botocore.exceptions.ClientError as e:
    if e.response['Error']['Code'] == "404":
        print("The object does not exist.")
    else:
        raise
