# Getting Started
This guide will help you set up S3 and use it with Boto3 and SDK for Javascript

##Prerequisites
In order to use Amazon S3, you need to create an account with AWS. You can [create a AWS free Tier account here.](https://aws.amazon.com/free/)
You need to install Boto3 and AWS SDK for javascript for Node.js
```bash
pip install boto3
```
```bash
npm install aws-sdk
```
##Configuration of files
To set up authentication credentials, sign into your AWS account and go to the [IAM Console.](https://console.aws.amazon.com/iam/home)
On the left side menu select Users, then Add User and fill out a username with programmatic access.
To set permissions for user, select Attach existing policies directly. For now you can choose Administrator Access and finish creating user. You should now be able to view your Access key ID and Secret Access key.

```bash
#Set up credentials in ~/.aws/credentials
[default]
aws_access_key_id = YOUR_ACCESS_KEY
aws_secret_access_key = YOUR_SECRET_KEY
```
Then set up a default region in ~/.aws/config
```bash
[default]
region=us-east-1
```
##Clone this repository and go through all the files to edit in your created bucket name. Run the programs and check your AWS account for reflected changes.

##Class Exercise
1)For the s3.py file, add a method that would download objects from your bucket.
2) Implement the same task as 1 but using AWS SDK this time instead of Boto3
