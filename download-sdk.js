var AWS = require('aws-sdk');
var fs =  require('fs');


var s3 = new AWS.S3({apiVersion: '2006-03-01'});
var params = {Bucket: 'bucket-name-here', Key: 'logo.png'};
var file = require('fs').createWriteStream('file.jpg');
s3.getObject(params).createReadStream().pipe(file);
