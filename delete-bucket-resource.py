#import boto3 library
import boto3
#create s3 object from boto3
s3 = boto3.resource('s3')

bucket = s3.Bucket('bucket-name-here)
#bucket must be empty first in order to delete bucket
#for loop iterates and deletes all objects
for key in bucket.objects.all():
    key.delete()
bucket.delete()

for bucket in s3.buckets.all():
    print(bucket.name)
