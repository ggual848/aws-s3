# Introduction to S3 #

```bash
# Clone this repository
git clone https://bitbucket.org/ggual848/aws-s3.git
```
[Getting Started with S3 Guide](GettingStarted.md)

# Further resources
[AWS SDK Documentation](https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html)
[Boto Documentation](http://boto3.readthedocs.io/en/latest/reference/services/s3.html)
