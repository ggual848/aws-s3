import boto3
#using client
s3 = boto3.client('s3')

s3.create_bucket(Bucket="bucket-name-here",ACL='public-read-write'),

response = s3.list_buckets()
buckets = [bucket['Name'] for bucket in response['Buckets']]

print("Bucket List: %s" % buckets)

filename = 'emoji.png'
bucket_name = 'bucket-name-here'

# Uploads the given file using a managed uploader, which will split up large
# files automatically and upload parts in parallel.
s3.upload_file(filename, bucket_name, filename,ExtraArgs={'ACL':'public-read'})

#download file with download_fileobj

print ("Done")

with open('downloaded.png', 'wb') as data:
    s3.download_fileobj(bucket_name, filename, data)
print ("Downloaded")
